const mongoose = require("mongoose");
const { Schema, model } = mongoose

//schema for category collection
const categorySchema = Schema({ categoryType: { type: String, unique: true, required: true, lowercase: true } });

// schema for productlist collection
const productScheama = Schema({
    productName: { type: String },
    productDesc: { type: String },
    category: { type: Schema.Types.ObjectId, ref: 'Category' }
})

const categoryModel = model("Category", categorySchema)
const productModel = model("Product", productScheama);
//function Productdetails Category wise
const getCategorydetails = async () => {
    var Category_Details = [];

    const category = await categoryModel.aggregate([{
        $lookup: {
            from: "products",
            localField: "_id",
            foreignField: "category",
            as: "products"
        }
    },
    {
        $project: { "categoryType": 1, "_id": 0, "productList": "$products.productName", "productCount": { $size: "$products" } }


    }]);
    return category;

}
//function deleteing Category
const deleteCategory = async (data) => {
    const category = await categoryModel.findOneAndRemove({ _id: data });
    if (category == null) {
        return "No Category"
    }
    else {
        const deleteProduct = await productModel.deleteMany({ category:category._id });
        return "Category : " + data + "  Deleted Associted Product Also deleted";
    }
}

//function for pagination
const productListPagination = async (page) => {
    const pageno = parseInt(page);
    const skip = 5 * (pageno - 1);
    const category = await productModel.aggregate([{
        $lookup: {
            from: "categories",
            localField: "category",
            foreignField: "_id",
            as: "details"
        }
    }, { $skip: skip }, { $limit: 5 }]);
    return category;
}
//fucntion similarliy
const testing = (callback) => {
    const category = categoryModel.aggregate([{
        $lookup: {
            from: "products",
            localField: "_id",
            foreignField: "category",
            as: "products"
        }
    },
    {
        $project: { "categoryType": 1, "_id": 0, "productList": "$products.productName", "productCount": { $size: "$products" } }


        }]).then(result => {
            callback(null, result);
        }).catch(err => {
            callback(null, err);
        })

}
const testing1 = (callback) => {
    const category = productModel.aggregate([{
        $lookup: {
            from: "categories",
            localField: "category",
            foreignField: "_id",
            as: "details"
        }
    }]).then(result => {
        callback(null, result);
    }).catch(err => {
        callback(null, err)
    });
}
//function for searching
const search = async (data) => {
    const category = await categoryModel.aggregate([{ "$match": { _id: mongoose.Types.ObjectId(data) } }, {
        $lookup: {
            from: "products",
            localField: "_id",
            foreignField: "category",
            as: "products"
        }
    },
    {
        $project: { "categoryType": 1, "_id": 0, "productList": "$products.productName", "productCount": { $size: "$products" } }


    }]);
    if (category.length==0) {
        return "No Category Found"
    }
    else {
        return category;
    }

}
/*
function addProduct()
{   const category=new categoryModel({categoryType:"furniture"});
    //const category=new productModel({productName:"table",productDesc:"Bouncer ......",category:"62330dc9fcd088eebddd3c85"});
    category.save();
    console.log("product added");
}
*/
//addProduct();
module.exports = { getCategorydetails, deleteCategory, productListPagination, testing, testing1 ,search};