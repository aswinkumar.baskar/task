const express = require("express")
const apiController = require("../controller/controller");
const { validation } = require("../validations/validation");
const { schema } = require("../validations/schema");
const { routelimitter } = require("../booster/rateLimiter");
const { route } = require("express/lib/application");
const router = express.Router();

// route for Getcategory Detaiils
router.get("/getCategorydetails", routelimitter, apiController.getCategorydetails);
//route for deletecategory
router.delete("/deleteCategory", validation(schema), apiController.deleteCategory)
//route for getproductlist
router.get("/productlist/:pageno", apiController.productListPagination);
//route for testing async
router.get("/testing", apiController.testingAsync);
//route for testing async series
router.get("/testingseries", apiController.testingseries);

//route for search categorywise
router.get("/search/:categoryName", apiController.searchCategory)
module.exports = { router }