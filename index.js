const express = require("express")
const mongoose = require("mongoose");
const { router } = require("./routes/router");
const { appLimitter } = require("./booster/rateLimiter");
require("dotenv").config();
const app = express();
// DB connection
mongoose.connect(process.env.DB_HOST, (err) => {
    if (err) throw err;
    console.log("DB connceted");
})
app.use(express.json());
//proxy setting
app.use(appLimitter)
app.set("trust proxy", 1);
app.use("/category", router);

app.listen(process.env.PORT, () => {
    console.log("server Running..!1")
})