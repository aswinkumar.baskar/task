const rateLimit = require("express-rate-limit");
const { MemoryStore } = rateLimit;
const options = { windowMs: 5 * 60 * 1000, max: 5, standardHeaders: true, legacyHeaders: false, store: new MemoryStore() }

const appOptions = { windowMs: 5 * 60 * 1000, max: 10, standardHeaders: true, legacyHeaders: false, store: new MemoryStore() }
const routelimitter = rateLimit(options);

const appLimitter = rateLimit(appOptions)

module.exports = { routelimitter, appLimitter }