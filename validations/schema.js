const yup = require("yup")

//schema For Delete Category API Request
const schema = yup.object({ id: yup.string().trim().required() });


module.exports = { schema };
