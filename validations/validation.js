// function request validation
const validation = (schema) => async (req, res, next) => {
    try {
        await schema.validate(req.body);
        return next();
    }
    catch (exception) {
        res.status(400).json({ error: exception.name, message: exception.message })
    }
}

module.exports = { validation }
