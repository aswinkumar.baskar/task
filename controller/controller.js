const Product = require("../model/model");
const async = require("async")
//controller for get Product Details Categorywise
const getCategorydetails = async (req, res) => {
    try {


        const product = await Product.getCategorydetails();
        res.status(200).json({ "Status": 1, "IP": req.ip, Details: product })
    }
    catch (err) {
        res.status(400).json({ "Status": 0, "message": "from catch", Error: err })
    }
}

//controller for deleting category
const deleteCategory = async (req, res) => {
    try {
        const product = await Product.deleteCategory(req.body.id);
        res.status(200).json({ "Status": 1, Messge: product })
    }
    catch (err) {
        res.status(400).json({ "Status": 0, Error: err })
    }

}
//controller for pagination
const productListPagination = async (req, res) => {
    try {


        const product = await Product.productListPagination(req.params.pageno);
        res.status(200).json({ "Status": 1, productList: product })
    }
    catch (err) {
        res.status(400).json({ "Status": 0, Error: err })
    }
}
//controller for async waterfall
const testingAsync = (req, res) => {
    try {
        async.waterfall([Product.testing], (err, result) => {
            if (err) throw Error(err);
            res.status(200).json({ "Status": 1, productList: result });
        })
    }
    catch (err) {
        res.status(400).json({ "status": 0, Error: err });
    }
}
//contorller for async series
const testingseries = (req, res) => {
    try {
        async.series([Product.testing, Product.testing1], (err, data) => {
            if (err) throw Error(err)
            res.status(200).json({ "Status": 1, productList: data });
        })
    }
    catch (err) {
        res.status(400).json({ "status": 0, Error: err });
    }
}

const searchCategory = async (req, res) => {
    try {
        const product = await Product.search(req.params.categoryName);

            
        res.status(200).json({ "Status": 1, productList: product })
    }
    catch (err) {
        res.status(400).json({ "status": 0, Error: err });
    }
}
module.exports = { getCategorydetails, deleteCategory, productListPagination, testingAsync, testingseries,searchCategory};